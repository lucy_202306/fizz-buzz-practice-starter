package tdd.fizzbuzz;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FizzBuzzTest {
    @Test
    void should_return_normal_number_when_countOff_given_normal_number() {

        int number = 1;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String result = fizzBuzz.countOff(number);

        Assertions.assertEquals("1", result);
    }


    @Test
    void should_return_Fizz_when_countOff_given_multiple_of_3() {
        int number = 12;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String result = fizzBuzz.countOff(number);

        Assertions.assertEquals("Fizz", result);

    }

    @Test
    void should_return_Buzz_when_countOff_given_multiple_of_5() {
        int number = 20;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String result = fizzBuzz.countOff(number);

        Assertions.assertEquals("Buzz", result);

    }
    @Test
    void should_return_FizzBuzz_when_countOff_given_multiple_of_3_and_5() {
        int number = 15;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String result = fizzBuzz.countOff(number);

        Assertions.assertEquals("FizzBuzz", result);

    }
    @Test
    void should_return_Whizz_when_countOff_given_multiple_of_7() {
        int number = 49;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String result = fizzBuzz.countOff(number);

        Assertions.assertEquals("Whizz", result);

    }
    @Test
    void should_return_FizzWhizz_when_countOff_given_multiple_of_3_and_7() {
        int number = 21;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String result = fizzBuzz.countOff(number);

        Assertions.assertEquals("FizzWhizz", result);

    }
    @Test
    void should_return_BuzzWhizz_when_countOff_given_multiple_of_5_and_7() {
        int number = 35;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String result = fizzBuzz.countOff(number);

        Assertions.assertEquals("BuzzWhizz", result);

    }
    @Test
    void should_return_FizzBuzzWhizz_when_countOff_given_multiple_of_3_and_5_and_7() {
        int number = 105;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String result = fizzBuzz.countOff(number);

        Assertions.assertEquals("FizzBuzzWhizz", result);

    }
}
